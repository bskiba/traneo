# -*- coding: utf-8 -*-

import urllib2, urllib
import json
import xml.etree.ElementTree as ET

class MsoftAuthenticator(object):
	"""Authenticator module for MsoftTranslateService"""
	def __init__(self):
		self.auth_url = "https://datamarket.accesscontrol.windows.net/v2/OAuth2-13"
		self.client_id = "TraNeo"
		self.client_secret = "1OHNXQEZbTu35OVNhjsxYpwwXcPyPak3c90ZkTo8yVM="
		self.request_details = urllib.urlencode({
			"grant_type":"client_credentials",
			"client_id":self.client_id,
			"client_secret":self.client_secret,
			"scope":"http://api.microsofttranslator.com"})

	def get_token(self):
		"""Returns the acces token needed to use translator service."""
		request = urllib2.Request(self.auth_url, self.request_details)
		try:
			response = urllib2.urlopen(request)
			return json.loads(response.read())
		except urllib2.HTTPError as e:
			print "Exception: ", e
			return None

class MsoftTranslateService(object):
	"""Translation client using Msoft Translation Service."""
	LANGUAGES = {
		"english":"en",
		"polish":"pl",
		"spanish":"es",
		"german":"de",
		"french":"fr"
	}

	def __init__(self):
		self.service_url = "http://api.microsofttranslator.com/v2/Http.svc/Translate?%s"
		self.authenticator = MsoftAuthenticator()

	def get_languages(self):
		"""Returns the supported languages for this translator client."""
		return MsoftTranslateService.LANGUAGES.keys()

	def translate(self, text, source, destination):
		"""Returns the translation of the text from the source to destination language."""
		token = self.authenticator.get_token()
		if token == None:
			return None
		data_to_translate = {"text":text, "from":MsoftTranslateService.LANGUAGES[source], "to":MsoftTranslateService.LANGUAGES[destination]}
		request = urllib2.Request(self.service_url % urllib.urlencode(data_to_translate))
		request.add_header("Authorization", "Bearer " + token[u'access_token'])
		try:
			response = urllib2.urlopen(request)
			translation = response.read()
			# Uncomment this for local testing
			# translation = r'<string xmlns="http://schemas.microsoft.com/2003/10/Serialization/">cheval</string>'
			root = ET.fromstring(translation)
			return root.text
		except urllib2.HTTPError as e:
			print "Exception: ", e
			return None