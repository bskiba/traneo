#!/usr/bin/env python
# -*- coding: utf-8 -*-

from translateService import MsoftTranslateService 	

class Translator(object):
	"""Main translating module"""

	def __init__(self, translateService = MsoftTranslateService):
		self.source = "english"
		self.destination = "french"
		self.translationService = translateService()
		

	def set_source_language(self, language):
		"""Set source language for the translator"""
		if language not in self.translationService.get_languages():
			raise ValueError("Unknown language")
		self.source = language

	def set_destination_language(self, language):
		"""Sets the destination language for the translator."""
		if language not in self.translationService.get_languages():
			raise ValueError("Unknown language")
		self.destination = language

	def translate(self, text):
		"""Translates the given text."""
		return self.translationService.translate(text, self.source, self.destination)

if __name__ == '__main__':
	t = Translator()
	t.set_destination_language("french")
	t.set_source_language("english")
	text = t.translate("horse")
	print text
	