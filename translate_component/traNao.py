from naoqi import ALProxy
from naoqi import ALModule
from naoqi import ALBroker
import translate
import threading
import sys
import time

from speechReco import MockWordlistReco
from speechReco import WordlistSpeechReco

LOCAL_IP = "192.168.1.100"
NAO_IP = "169.254.113.113"

IP = NAO_IP
PORT = 9559

global myBroker
global TraNao
global Reco

myBroker = None
TraNao = None
memory = None
Reco = None

class TraNaoModule(ALModule):
	"""Main component for robot translate actions.
		Also hosts main workflow for translation component."""
	GREETING = "Hello, my name is TraNeo. I'm your new best translator. What can I do for you? Say choices for help."
	CHOICES = ["choices","language","translate", "settings", "bye"]
	HELP = "You can say language to set source and destination languages."\
		+ " You can say translate to proceed with translation. "\
		+ " You can say settings to get currently set languages. "\
		+ "If you want to finish just say bye. "\
		+ "You can 	also say choices to hear this again."
	SET_SOURCE ="Tell me the source language please."
	SET_DESTINATION ="Tell me the destination language please."
	LANGUAGE_CONFIRM = "I have set languages. If you want to start translating words please say translate."
	SETTINGS = "Current settings. Source language - {0}. Destination language - {1}"
	TRANSLATE = "translate"
	GET_CHOICES = "choices"
	SET_LANGUAGE = "language"
	GET_SETTINGS = "settings"
	EXIT = "bye"
	NAME = "TraNao"
	LANGUAGES = ["English", "French"]
	WORDS = ["horse", "pear", "bird", "book", "concrete"]
	
	def __init__(self, broker):
		
		ALModule.__init__(self, TraNaoModule.NAME)
		self.broker = broker
		
		self.tts = ALProxy("ALTextToSpeech")
		self.tts.setLanguage("English")
		
		self.translator = translate.Translator()
		
		global Reco

		# Reco = MockWordlistReco()

		Reco = WordlistSpeechReco(self.broker)

		self.reco = Reco

	def greet(self):
		"""Greets the user."""
		self.tts.say(TraNaoModule.GREETING)
		
	def choices(self):
		"""Menu for the basic workflow. 
			Waits for the menu command and acts appropriately."""
		self.reco.set_word_list(TraNaoModule.CHOICES)
		command = self.recognize_word()

		if command == TraNaoModule.TRANSLATE:
			self.translate_text()
			return True
		elif command == TraNaoModule.GET_CHOICES:
			self.help()
			return True
		elif command == TraNaoModule.SET_LANGUAGE:
			self.set_source()
			self.set_destination()
			self.confirm_languages()
			return True
		elif command == TraNaoModule.GET_SETTINGS:
			self.get_settings()
			return True
		elif command == TraNaoModule.EXIT:
			return False
		else:
			self.choices()
		
	def set_source(self):
		"""Asks user for input and sets the source language."""
		self.tts.say(TraNaoModule.SET_SOURCE)
		self.reco.set_word_list(TraNaoModule.LANGUAGES)
		language = self.recognize_word()
		self.source = language
			
	def set_destination(self):
		"""Asks user for input and sets the destination language."""
		self.tts.say(TraNaoModule.SET_DESTINATION)
		self.reco.set_word_list(TraNaoModule.LANGUAGES)
		language = self.recognize_word()
		self.destination = language
	
	def confirm_languages(self):
		self.tts.say(TraNaoModule.LANGUAGE_CONFIRM)

	def help(self):
		""" Say help text."""
		self.tts.say(TraNaoModule.HELP)
	
	def translate_text(self):
		"""Start listening for the world to translate."""	
		self.reco.set_word_list(TraNaoModule.WORDS)
		text = self.recognize_word()
		translation = self.translator.translate(text)
		self.tts.setLanguage(self.translator.destination)
		self.tts.say(translation)
		self.tts.setLanguage("english")

	def basic_workflow(self):
		# self.greet()
		while (self.choices()):
			pass
		
	def mockTranslate(self):
		"""Mock translate method for testing purposes"""
		text = "horse"
		translation = self.translator.translate(text)
		self.tts.setLanguage(self.translator.destination)
		self.tts.say(translation)
		self.tts.setLanguage("english")

	def recognize_word(self):
		text = self.reco.recognize()
		while text == None:
			self.tts.say("Could you repeat?")
			text = self.reco.recognize()
		return text

	def get_settings(self):
		settings_string = TraNaoModule.SETTINGS.format(self.translator.source, self.translator.destination)
		self.tts.say(settings_string)

	def terminate(self):
		myBroker.shutdown()

import signal
import sys
def signal_handler(signal, frame):
    memory.unsubscribeToEvent("WordRecognized", 
		WordlistSpeechReco.NAME)

if __name__ == "__main__":

	signal.signal(signal.SIGINT, signal_handler)

	myBroker = ALBroker("myBroker",
   		"0.0.0.0",   # listen to anyone
   		0,           # find a free port and use it
   		IP,         # parent broker IP
   		PORT)       # parent broker port
	
	tm = TraNaoModule(myBroker)
	try:
		tm.basic_workflow()
	except Exception as e:
		print e
	finally:
		tm.terminate()
	
	
	
	